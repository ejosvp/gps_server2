#include "plotmanager.h"
#include <QSqlQuery>
#include <QVariant>
#include <QSqlError>

PlotManager::PlotManager()
{
}

QString serialize(Info info)
{
    QString s("a:");
    s += QString::number(info.size());
    s += ":{";

    Info::iterator i;
    for (i = info.begin(); i != info.end(); ++i) {
        s += "s:" + QString::number(i.key().length()) + ":\"" + i.key() + "\";";
        s += "s:" + QString::number(i.value().length()) + ":\"" + i.value() + "\";";
    }

    s += "}";

    return s;
}

bool PlotManager::save(Plot &plot, QSqlDatabase &db)
{
    QSqlQuery query(db);

    query.prepare(
        "INSERT INTO Trama"
        "(tracker_id, serverTime, timeStamp, valid, latitude, longitude, altitude, speed, course, address, extendedInfo) VALUES "
        "(:tracker_id, :serverTime, :timeStamp, :valid, :latitude, :longitude, :altitude, :speed, :course, :address, :extendedInfo)");

    query.bindValue(":tracker_id",  QString::number(plot.getDeviceId()));
    query.bindValue(":serverTime",  plot.getServerTime().toString("yyyy-MM-dd hh:mm:ss"));
    query.bindValue(":timeStamp",   plot.getTimestamp().toString("yyyy-MM-dd hh:mm:ss"));
    query.bindValue(":valid",       plot.isValid());
    query.bindValue(":latitude",    plot.getLatitude());
    query.bindValue(":longitude",   plot.getLongitude());
    query.bindValue(":altitude",    plot.getAltitude());
    query.bindValue(":speed",       plot.getSpeed());
    query.bindValue(":course",      plot.getCourse());
    query.bindValue(":address",     plot.getAddres());
    query.bindValue(":extendedInfo", serialize(plot.getExtendedInfo()));

    query.exec();

    if (query.lastError().isValid()){
        qFatal(query.lastError().text().toStdString().c_str());
    }

    return true;
}
