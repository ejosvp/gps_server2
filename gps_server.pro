#-------------------------------------------------
#
# Project created by QtCreator 2013-10-27T18:33:42
#
#-------------------------------------------------

QT       += core network sql

QT       -= gui

TARGET = gps_server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    plot.cpp \
    protocol/meiligao.cpp \
    plotmanager.cpp \
    devicemanager.cpp

HEADERS += \
    server.h \
    plot.h \
    protocol/meiligao.h \
    plotmanager.h \
    devicemanager.h
