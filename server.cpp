#include "server.h"
#include <QDebug>
#include <QByteArray>
#include <QSqlError>

#include "protocol/meiligao.h"
#include "plotmanager.h"
#include "devicemanager.h"

Server::Server(QObject *parent) : QObject(parent)
{
    QObject::connect(&tcpServer, SIGNAL(newConnection()), this, SLOT(acceptConnection()));

    db = QSqlDatabase::addDatabase("QMYSQL", "gps");
    db.setHostName("localhost");
    db.setDatabaseName("gps");
    db.setUserName("root");
    db.setPassword("");
    if (!db.open()) {
        qDebug() << db.lastError();
    }
}

Server::~Server() {}

void Server::start(quint16 port)
{
    while (!tcpServer.isListening() && !tcpServer.listen(QHostAddress::LocalHost, port != 0 ? port : 8080)) {
        qDebug() << "Error: " + tcpServer.errorString();
    }
    qDebug() << "Server running on " + tcpServer.serverAddress().toString() + ":" + QString::number(tcpServer.serverPort());
}

void Server::acceptConnection()
{
    tcpServerConnection = tcpServer.nextPendingConnection();
    QObject::connect(tcpServerConnection, SIGNAL(readyRead()),
            this, SLOT(updateServerProgress()));
    QObject::connect(tcpServerConnection, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));
}

void Server::updateServerProgress()
{
    QByteArray all = tcpServerConnection->readLine();

    if (!all.isEmpty()) {
        Plot p;
        Meiligao mei;

        if (mei.decode(all, p)) {
            // Error with date of plot
            p.setDeviceId(DeviceManager::findByIMEI("1", db));
            p.setTimestamp(QDateTime::currentDateTime());
            if (PlotManager::save(p, db)) qDebug() << "saved!";
        }
    }
}

void Server::displayError(QAbstractSocket::SocketError socketError)
{
    if (socketError == QTcpSocket::RemoteHostClosedError) {
        return;
    }

    qDebug() << "Network error: The following error occurred: " + tcpClient.errorString() + ".";
}
