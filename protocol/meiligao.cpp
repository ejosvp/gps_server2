#include "meiligao.h"
#include <iostream>
#include <QDateTime>

const short HEADER_SIZE = 13;
const short ENDING_SIZE = 4;

Meiligao::Meiligao()
{
    pattern = QRegularExpression(
                "(\\d{2})(\\d{2})(\\d{2})\\.?(\\d+)?,"      // Time (HHMMSS.SSS)
                "([AV]),"                                   // Validity
                "(\\d+)(\\d{2}\\.\\d+),"                    // Latitude (DDMM.MMMM)
                "([NS]),"
                "(\\d+)(\\d{2}\\.\\d+),"                    // Longitude (DDDMM.MMMM)
                "([EW]),"
                "(\\d+.\\d+),"                              // Speed
                "(\\d+\\.?\\d*)?,"                          // Course
                "(\\d{2})(\\d{2})(\\d{2})"                  // Date (DDMMYY)
                "(?:[^\\|]*\\|(\\d+\\.\\d+)\\|"             // Dilution of precision
                "(\\d+\\.?\\d*)\\|)?"                       // Altitude
                "([0-9a-fA-F]+)?"                           // State
                "(?:\\|([0-9a-fA-F]+),([0-9a-fA-F]+))?"     // ADC
                "(?:,([0-9a-fA-F]+),([0-9a-fA-F]+)"
                ",([0-9a-fA-F]+),([0-9a-fA-F]+)"
                ",([0-9a-fA-F]+),([0-9a-fA-F]+))?"
                "(?:\\|([0-9a-fA-F]+))?"                    // Cell
                "(?:\\|([0-9a-fA-F]+))?"                    // Signal
                "(?:\\|([0-9a-fA-F]+))?"                    // Milage
                ".*");
}

QString Meiligao::getId(QByteArray buf)
{
    int id = 0;

    for (int i = 3; i < 10; i++) {
        int b = buf.at(i);

        // First digit
        int d1 = (b & 0xf0) >> 4;
        if (d1 == 0xf) break;
        id += d1;

        // Second digit
        int d2 = (b & 0x0f);
        if (d2 == 0xf) break;
        id += d2;
    }

    return QString::number(id);
}

bool Meiligao::decode(QByteArray buf, Plot &plot)
{
    Info extendedInfo;

    QString id = getId(buf);
    try {
        plot.setDeviceId(1); // todo: fix getId parser
    } catch (std::exception &e) {
        qFatal("Unknown device");
    }

    QString data = buf.mid(HEADER_SIZE, buf.size() - HEADER_SIZE - ENDING_SIZE).constData();
    QRegularExpressionMatch match = pattern.match(data);

    if (!match.hasMatch()) return false;

    int index = 1;

    // Time
    int th = match.captured(index++).toInt();
    int tm = match.captured(index++).toInt();
    int ts = match.captured(index++).toInt();
    int tms = match.captured(index++).toInt();
    QTime plot_time(th, tm, ts, tms);

    // Validity
    plot.setValid(match.captured(index++).compare("A") == 0);

    // Latitude
    double latitude = match.captured(index++).toDouble();
    latitude += match.captured(index++).toDouble() / 60;
    if (match.captured(index++).compare("S") == 0)
        latitude = -latitude;
    plot.setLatitude(latitude);

    // Longitude
    double longitude = match.captured(index++).toDouble();
    longitude += match.captured(index++).toDouble() / 60;
    if (match.captured(index++).compare("W") == 0)
        longitude = -longitude;
    plot.setLongitude(longitude);

    // Speed
    plot.setSpeed(match.captured(index++).toDouble());

    // Course
    plot.setCourse(match.captured(index++).toDouble());

    // Date
    int dy = match.captured(index++).toInt();
    int dm = match.captured(index++).toInt();
    int dd = match.captured(index++).toInt();
    QDate plot_date(dy, dm, dd);
    plot.setTimestamp(QDateTime(plot_date, plot_time));

    // Dilution of precision
    extendedInfo.insert("hdop", match.captured(index++));

    // Altitude
    plot.setAltitude(match.captured(index++).toDouble());

    // State
    extendedInfo.insert("state", match.captured(index++));

    // ADC
    for (int i = 1; i <= 8; i++) {
        QString adc = match.captured(index++);
        if (!adc.isNull())
            extendedInfo.insert("adc" + QString::number(i), adc);
    }

    // Cell identifier
    QString cell = match.captured(index++);
    if (!cell.isNull())
        extendedInfo.insert("cell", cell);

    // GSM signal
    QString gsm = match.captured(index++);
    if (!cell.isNull())
        extendedInfo.insert("gsm", gsm);

    // Milage
    QString milage = match.captured(index++);
    if (!milage.isNull())
        extendedInfo.insert("milage", milage);

    plot.setExtendedInfo(extendedInfo);

    return true;
}
