#ifndef MEILIGAO_H
#define MEILIGAO_H

#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "plot.h"

class Meiligao
{
private:
    QRegularExpression pattern;

public:
    QString getId(QByteArray);
    bool decode(QByteArray, Plot&);
    Meiligao();

};

#endif // MEILIGAO_H
