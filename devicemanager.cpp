#include "devicemanager.h"

#include <QSqlQuery>
#include <QSqlResult>

DeviceManager::DeviceManager()
{
}

long DeviceManager::findByIMEI(QString imei, QSqlDatabase &db)
{
    QSqlQuery query(db);

    query.prepare("SELECT id FROM Tracker WHERE codigo=:codigo");

    query.bindValue(":codigo", imei);

    query.exec();

    return query.boundValue(0).toUInt();
}
