#include <QCoreApplication>
#include "server.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Server s;

    quint16 port;

    if (argc > 1) {
        port = QString::fromStdString(argv[1]).toUInt();
    }

    s.start(port);

    return a.exec();
}
