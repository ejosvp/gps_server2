#include "plot.h"

Plot::Plot()
{
    serverTime = QDateTime::currentDateTime();
}

void Plot::setDeviceId(long deviceId)
{
    this->deviceId = deviceId;
}

void Plot::setServerTime(QDateTime serverTime)
{
    this->serverTime = serverTime;
}

void Plot::setTimestamp(QDateTime timestamp)
{
    this->timestamp = timestamp;
}

void Plot::setValid(bool valid)
{
    this->valid = valid;
}

void Plot::setLatitude(double latitude)
{
    this->latitude = latitude;
}

void Plot::setLongitude(double longitude)
{
    this->longitude = longitude;
}

void Plot::setAltitude(double altitude)
{
    this->altitude = altitude;
}

void Plot::setSpeed(double speed)
{
    this->speed = speed;
}

void Plot::setCourse(double course)
{
    this->course = course;
}

void Plot::setAddres(QString address)
{
    this->address = address;
}

void Plot::setExtendedInfo(Info extendedInfo)
{
    this->extendedInfo = extendedInfo;
}
