#ifndef PLOTMANAGER_H
#define PLOTMANAGER_H

#include <QSqlDatabase>
#include "plot.h"

class PlotManager
{
public:
    PlotManager();
    static bool save(Plot &, QSqlDatabase &);
};

#endif // PLOTMANAGER_H
