#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include <QString>
#include <QSqlDatabase>

class DeviceManager
{
public:
    DeviceManager();
    static long findByIMEI(QString, QSqlDatabase &);
};

#endif // DEVICEMANAGER_H
