#ifndef PLOT_H
#define PLOT_H

#include <QDateTime>

typedef QHash<QString, QString> Info;

class Plot
{
private:
    long id;
    long deviceId;

    QDateTime serverTime;

    QDateTime timestamp;
    bool valid;
    double latitude;
    double longitude;
    double altitude;
    double speed;
    double course;
    QString address;

    Info extendedInfo;

    void setId(long);

public:
    Plot();

    void setDeviceId(long);

    void setServerTime(QDateTime);

    void setTimestamp(QDateTime);
    void setValid(bool);
    void setLatitude(double);
    void setLongitude(double);
    void setAltitude(double);
    void setSpeed(double);
    void setCourse(double);
    void setAddres(QString);
    void setExtendedInfo(Info);

    inline long getId() { return id; }
    inline long getDeviceId() { return deviceId; }

    inline QDateTime getServerTime() { return serverTime; }
    inline QDateTime getTimestamp() { return timestamp; }
    inline bool isValid() { return valid; }
    inline double getLatitude() { return latitude; }
    inline double getLongitude() { return longitude; }
    inline double getAltitude() { return altitude; }
    inline double getSpeed() { return speed; }
    inline double getCourse() { return course; }
    inline QString getAddres() { return address; }
    inline Info getExtendedInfo() { return extendedInfo; }
};

#endif // PLOT_H
